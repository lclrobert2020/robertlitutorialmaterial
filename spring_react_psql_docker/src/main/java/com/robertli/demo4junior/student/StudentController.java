package com.robertli.demo4junior.student;

import com.robertli.demo4junior.exception.ApiRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("api/students")
public class StudentController {

    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> getAllStudents(){
        return studentService.getAllStudents();
    }

    @PostMapping
    public void addNewStudent(@RequestBody @Valid Student student){
        studentService.addNewStudent(student);
    }

    @GetMapping(path="{studentId}/courses")
    public List<StudentCourse> getAllCourseForStudent(@PathVariable("studentId") UUID studentId){
        return  studentService.getAllCourseForStudent(studentId);
    }

}

package com.robertli.demo4junior.student;

import com.robertli.demo4junior.exception.ApiRequestException;
import com.robertli.demo4junior.ulit.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class StudentService {

    private final StudentDataAccessService studentDataAccessService;

    private final EmailValidator emailValidator;

    @Autowired
    public StudentService(StudentDataAccessService studentDataAccessService, EmailValidator emailValidator) {
        this.studentDataAccessService = studentDataAccessService;
        this.emailValidator = emailValidator;
    }

    public List<Student> getAllStudents(){
        return studentDataAccessService.selectAllStudents();
    };

    public void addNewStudent(Student student) {
        addNewStudent(null, student);
    }

    public void addNewStudent(UUID studentId, Student student) {
       UUID newStudentId = Optional.ofNullable(studentId).orElse(UUID.randomUUID());
       // validate email format
       if(!emailValidator.isValidEmailAddress(student.getEmail())){
            throw new ApiRequestException(student.getEmail()+" is not valid");
       }
        // validate if email is taken
       if(studentDataAccessService.isEmailTaken(student.getEmail())){
           throw new ApiRequestException(student.getEmail()+ " is taken already");
       }
       studentDataAccessService.insertStudent(newStudentId, student);
    }

    public List<StudentCourse> getAllCourseForStudent(UUID studentId) {
        return  studentDataAccessService.selectAllStudentsCourse(studentId);
    }
}

package com.robertli.demo4junior.ulit;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class EmailValidatorTest {

    private final EmailValidator underTest = new EmailValidator();

    @Test
    void itShouldValidateCorrectEmail() {
        assertThat(underTest.isValidEmailAddress("hello@gmail.com")).isTrue();
    }

    @Test
    void itShouldValidateIncorrectEmail() {
        assertThat(underTest.isValidEmailAddress("hellogmail.com")).isFalse();
    }

    @Test
    void itShouldValidateIncorrectEmailWithoutDotAtTheEnd() {
        assertThat(underTest.isValidEmailAddress("hello@gmail")).isFalse();
    }
}
import React, { Component } from "react";
import { Formik } from "formik";
import { Input, Button, Tag, Radio } from "antd";
import { addNewStudents } from "../client";

const inputBottomMargin = { marginBottom: "10px" };
const tagStyle = {
  backgroundColor: "#f50",
  color: "white",
  ...inputBottomMargin,
};
const handleValidate = (values) => {
  const errors = {};
  if (!values.email) {
    errors.email = "Required";
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
    errors.email = "Invalid email address";
  }
  if (!values.password) {
    errors.password = "Required";
  }
  if (!values.firstName) {
    errors.firstName = "Required";
  }
  if (!values.lastName) {
    errors.lastName = "Required";
  }
  if (!values.gender) {
    errors.gender = "Required";
  }
  if (["MALE", "FEMALE", "male", "female"].includes(values)) {
    errors.gender = "Gender must be (MALE, male. FEMALE, female)";
  }
  return errors;
};

const AddStudentForm = (props) => (
    <Formik
      initialValues={{
        email: "",
        password: "",
        firstName: "",
        lastName: "",
        gender: "",
      }}
      validate={handleValidate}
      onSubmit={async (values, { setSubmitting, resetForm }) => {
        try {
          let response = await addNewStudents(values);
          resetForm();
          props.onSuccess("Success", "Successfully added students");
        } catch (err) {
          props.onFailure(err.error.message, err.error.httpStatus);
        } finally {
          setSubmitting(false);
        }
      }}
    >
      {({
        values,
        errors,
        touched,
        handleChange,
        handleBlur,
        handleSubmit,
        isSubmitting,
        submitForm,
      }) => {
        return (
          <form onSubmit={handleSubmit}>
            <Input
              style={inputBottomMargin}
              name="firstName"
              onBlur={handleBlur}
              onChange={(e) => {
                handleChange(e);
              }}
              value={values.firstName}
              placeholder="First Name. E.g John"
            />
            {errors.firstName && touched.firstName && (
              <Tag style={tagStyle}>{errors.firstName}</Tag>
            )}
            <Input
              style={inputBottomMargin}
              name="lastName"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.lastName}
              placeholder="Last Name. E.g Lee"
            />
            {errors.lastName && touched.lastName && (
              <Tag style={tagStyle}>{errors.lastName}</Tag>
            )}
            <Input
              style={inputBottomMargin}
              name="email"
              type="email"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              placeholder="Email. E.g example@gmail.com"
            />
            {errors.email && touched.email && (
              <Tag style={tagStyle}>{errors.email}</Tag>
            )}
            <div>
              <Radio.Group
                style={inputBottomMargin}
                name="gender"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.gender}
              >
                <Radio value={"MALE"}>MALE</Radio>
                <Radio value={"FEMALE"}>FEMALE</Radio>
              </Radio.Group>
              {errors.gender && touched.gender && (
                <Tag style={tagStyle}>Required</Tag>
              )}
            </div>
            <Input
              style={inputBottomMargin}
              type="password"
              name="password"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              placeholder="Password"
            />
            {errors.password && touched.password && (
              <Tag style={tagStyle}>{errors.password}</Tag>
            )}
            <Button
              type="submit"
              onClick={() => submitForm()}
              disabled={isSubmitting}
            >
              Submit
            </Button>
          </form>
        );
      }}
    </Formik>
);

export default AddStudentForm;

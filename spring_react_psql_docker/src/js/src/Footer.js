import React from "react";
import Container from "./Container";
import { Avatar, Button } from "antd";
import "./Footer.css";
const Footer = (props) => (
  <div className="footer">
    <Container>
      {props.numberOfStudent !== undefined ? (
        <Avatar
          size="large"
          style={{ background: "#f56a00", marginRight: "5px" }}
        >
          {props.numberOfStudent}
        </Avatar>
      ) : null}
      <Button onClick={()=>props.handleAddStudentClickEvent()} type="primary">Add new student</Button>
    </Container>
  </div>
);

export default Footer;

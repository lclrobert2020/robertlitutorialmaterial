import { notification } from "antd";

const openNotification = (type, message, description) => {
  notification[type]({
    message: message,
    description: description,
  });
};

const successNotification = (message, description) =>
  openNotification("success", message, description);

const infoNotification = (message, description) =>
  openNotification("info", message, description);

const warnNotification = (message, description) =>
  openNotification("warn", message, description);

const errorNotification = (message, description) =>
  openNotification("error", message, description);


export {successNotification, infoNotification, warnNotification, errorNotification}
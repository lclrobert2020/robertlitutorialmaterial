import fetch from "unfetch";

const checkStatus = (response) => {
  return new Promise((resolve, reject) => {
    if (response.ok) return resolve(response);
    let error = new Error(response.statusText);
    error.response = response;
    response
      .json()
      .then((e) => (error.error = e))
      .finally(()=> reject(error));
  });
};

const getAllStudents = () => {
  return fetch("api/students").then(checkStatus);
};

const addNewStudents = (student) => {
  return fetch("api/students", {
    headers: {
      "Content-Type": "application/json",
    },
    method: "POST",
    body: JSON.stringify(student),
  }).then(checkStatus);
};

export { getAllStudents, addNewStudents };

import logo from "./logo.svg";
import { getAllStudents } from "./client";
import React, { useState, Component } from "react";
import Footer from "./Footer";
import { Table, Spin, Modal, Empty } from "antd";
import { LoadingOutlined } from "@ant-design/icons";
import Container from "./Container";
import AddStudentForm from "./forms/AddStudentForm";
import { errorNotification, successNotification } from "./Notification";
const getIndicatorIcon = () => {
  return <LoadingOutlined style={{ fontSize: 24 }} spin />;
};

const columns = [
  {
    title: "Student Id",
    dataIndex: "studentId",
    key: "studentId",
  },
  {
    title: "First Name",
    dataIndex: "firstName",
    key: "firstName",
  },
  {
    title: "Last Name",
    dataIndex: "lastName",
    key: "lastName",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Gender",
    dataIndex: "gender",
    key: "gender",
  },
];
class App extends Component {
  state = {
    students: [],
    isFetching: false,
    isAddStudentModalVisisble: false,
  };

  fetchStudents = () => {
    this.setState({
      isFetching: true,
    });
    getAllStudents()
      .then((res) => {
        return res.json();
      })
      .then((students) => {
        this.setState({ students, isFetching: false });
      })
      .catch((err) => errorNotification(err.error.message, err.error.httpStatus))
      .finally((_) => {
        this.setState({
          isFetching: false,
        });
      });
  };

  componentDidMount() {
    this.fetchStudents();
  }

  openAddStudentModal = () =>
    this.setState({ isAddStudentModalVisisble: true });

  closeStudentModal = () => this.setState({ isAddStudentModalVisisble: false });

  render() {
    if (this.state.isFetching) {
      return (
        <Container>
          <Spin indicator={getIndicatorIcon()} />
        </Container>
      );
    }

    return (
      <Container>
        {this.state.students && this.state.students.length ? (
          <>
            <Table
              style={{ paddingBottom: "100px" }}
              dataSource={this.state.students}
              columns={columns}
              rowKey={"studentId"}
              pagination={false}
            />
          </>
        ) : (
          <>
            <Empty description="No Students Found" />
          </>
        )}
        <Modal
          title="Add new student"
          visible={this.state.isAddStudentModalVisisble}
          onOk={this.closeStudentModal}
          onCancel={this.closeStudentModal}
          width={1000}
        >
          <AddStudentForm
            onSuccess={(message, description) => {
              successNotification(message, description);
              this.closeStudentModal();
              this.fetchStudents();
            }}
            onFailure={(message, description)=>{
              errorNotification(message, description);
            }}
          />
        </Modal>
        <Footer
          handleAddStudentClickEvent={this.openAddStudentModal}
          numberOfStudent={this.state.students.length}
        />
      </Container>
    );
  }
}

export default App;
